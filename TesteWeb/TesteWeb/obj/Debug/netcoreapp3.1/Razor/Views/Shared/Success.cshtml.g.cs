#pragma checksum "D:\Programas\Developer\Projetos\1. Curso C# .Net\TesteWeb\TesteWeb\Views\Shared\Success.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6478bd1e944fe607480095a256eb053132f9d29b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Success), @"mvc.1.0.view", @"/Views/Shared/Success.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Programas\Developer\Projetos\1. Curso C# .Net\TesteWeb\TesteWeb\Views\_ViewImports.cshtml"
using TesteWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Programas\Developer\Projetos\1. Curso C# .Net\TesteWeb\TesteWeb\Views\_ViewImports.cshtml"
using TesteWeb.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6478bd1e944fe607480095a256eb053132f9d29b", @"/Views/Shared/Success.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"de59540273f65d0623cb4bcf3267b43859a0506e", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Success : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TesteWeb.Models.SuccessViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            WriteLiteral("\r\n        <h1 class=\"text-success\">");
#nullable restore
#line 4 "D:\Programas\Developer\Projetos\1. Curso C# .Net\TesteWeb\TesteWeb\Views\Shared\Success.cshtml"
                            Write(Model.MsgSuccess);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</h1>
        
        <!--Temporizador da página após o cadastro e atualiza dinamicamente voltando pra index -->
        <script type=""text/javascript"">

            (function () {
                setTimeout(function ()
                {
                    window.location = ""https://localhost:44311"";
                }, 2000); /* 2000ms == 2 segundos*/
            })();

        </script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TesteWeb.Models.SuccessViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
