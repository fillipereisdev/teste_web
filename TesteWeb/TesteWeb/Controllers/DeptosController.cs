﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TesteWeb.Data;
using TesteWeb.Models;

namespace TesteWeb.Controllers
{
    public class DeptosController : Controller
    {
        private readonly TesteWebContext _context;
        public string msg;

        public DeptosController(TesteWebContext context)
        {
            _context = context;
        }

        // GET: Deptos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Depto.ToListAsync());
        }

        // GET: Deptos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depto = await _context.Depto
                .FirstOrDefaultAsync(m => m.Id == id);
            if (depto == null)
            {
                return NotFound();
            }

            return View(depto);
        }

        // GET: Deptos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Deptos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Departamento,Cargo")] Depto depto)
        {
            if (ModelState.IsValid)
            {
                _context.Add(depto);
                await _context.SaveChangesAsync();
                
            }
            return View(depto);
        }

        // GET: Deptos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depto = await _context.Depto.FindAsync(id);
            if (depto == null)
            {
                return NotFound();
            }
            return View(depto);
        }

        // POST: Deptos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Departamento,Cargo")] Depto depto)
        {
            if (id != depto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(depto);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeptoExists(depto.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(depto);
        }

        // GET: Deptos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depto = await _context.Depto
                .FirstOrDefaultAsync(m => m.Id == id);
            if (depto == null)
            {
                return NotFound();
            }

            return View(depto);
        }

        // POST: Deptos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var depto = await _context.Depto.FindAsync(id);
            _context.Depto.Remove(depto);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeptoExists(int id)
        {
            return _context.Depto.Any(e => e.Id == id);
        }
    }
}
