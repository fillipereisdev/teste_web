﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // DataAnnotation para validar formulário nas views.
using System.Linq;
using System.Threading.Tasks;

namespace TesteWeb.Models
{
    public class Product
    {
        public int Id { get; set; }
        
        public string Produto { get; set; }

        [DisplayFormat(DataFormatString = "R$ {0:N2}", ApplyFormatInEditMode = true)] // Formatação de casa decimal do preço.
        public double Preço { get; set; }
        
        public string OBS { get; set; }

    }
}