﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TesteWeb.Models
{
    public class SuccessViewModel
    {
        public string RequestId { get; set; }
        public string MsgSuccess { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}