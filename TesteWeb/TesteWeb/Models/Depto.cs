﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations; //Annotation para validar formulário nas views

namespace TesteWeb.Models
{
    public class Depto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Por gentileza, preencha seu departamento!")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "{0}: Intervalo entre {2} e {1} caracteres")]
        public string Departamento { get; set; }
        
        [Required(ErrorMessage = "Por gentileza, preencha seu cargo!")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "{0}: Intervalo entre {2} e {1} caracteres")]
        public string Cargo { get; set; }
    }
}
