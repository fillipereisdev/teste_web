using System;

namespace TesteWeb.Models
{
    public class ErrorViewModel // Classe de erros comuns sendo tratados
    {
        public string RequestId { get; set; }
        public string MsgError { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
