﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; //DataAnnotation para validar formulário nas views
using System.Linq;
using System.Threading.Tasks;

namespace TesteWeb.Models
{
    public class Seller
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Por gentileza, preencha seu nome!")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "{0}: Intervalo entre {2} e {1} caracteres")]
        public string Nome { get; set; }
        
        [Required(ErrorMessage = "Por gentileza, preencha seu email!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Por gentileza, preencha seu telefone ou celular!")]
        [Display(Name = "Tel/Cel ou WhatsApp")]
        [DataType(DataType.PhoneNumber)]
        public string Telefone { get; set; }
    }   
}