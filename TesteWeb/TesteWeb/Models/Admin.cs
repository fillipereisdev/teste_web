﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; //DataAnnotation para validar formulário nas views
using System.Linq;
using System.Threading.Tasks;

namespace TesteWeb.Models
{
    public class Admin
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Por gentileza, preencha seu email!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Por gentileza, preencha sua senha!")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "{0}: Intervalo entre {2} e {1} caracteres")]
        public string Senha { get; set; }
    }
}
