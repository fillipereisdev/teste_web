﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TesteWeb.Models;

namespace TesteWeb.Data
{
    public class TesteWebContext : DbContext
    {
        public TesteWebContext (DbContextOptions<TesteWebContext> options)
            : base(options)
        {
        }

        public DbSet<TesteWeb.Models.Seller> Seller { get; set; }

        public DbSet<TesteWeb.Models.Depto> Depto { get; set; }

        public DbSet<TesteWeb.Models.Product> Product { get; set; }
    }
}